<?php

namespace Artel\Support\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Arr;

class PostgresArray implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes)
    {
        $list = str_replace(['{', '}', '"'], '', $value);

        if (empty($list)) {
            return [];
        }

        return explode(',', $list);
    }

    public function set($model, string $key, $value, array $attributes)
    {
        $list = implode(',', $value);;

        if (empty($list)) {
            return '[]';
        }

        return "{{$list}}";
    }
}

