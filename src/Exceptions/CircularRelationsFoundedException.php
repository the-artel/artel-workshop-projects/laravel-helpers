<?php

namespace Artel\Support\Exceptions;

class CircularRelationsFoundedException extends EntityCreateException
{
}
