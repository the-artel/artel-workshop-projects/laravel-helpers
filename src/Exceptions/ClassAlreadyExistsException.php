<?php

namespace Artel\Support\Exceptions;

class ClassAlreadyExistsException extends EntityCreateException
{
}
