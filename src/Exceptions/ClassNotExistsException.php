<?php

namespace Artel\Support\Exceptions;

class ClassNotExistsException extends EntityCreateException
{
}
