<?php

namespace Artel\Support\Exceptions;

use Exception;

class IncorrectImportFileException extends Exception
{
}
