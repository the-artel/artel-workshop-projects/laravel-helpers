<?php

namespace Artel\Support\Exceptions;

use Exception;

class IncorrectImportLineException extends Exception
{
}
