<?php

namespace Artel\Support\Exceptions;

use Exception;

class InvalidModelException extends Exception
{
}
