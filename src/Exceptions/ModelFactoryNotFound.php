<?php

namespace Artel\Support\Exceptions;

class ModelFactoryNotFound extends EntityCreateException
{
}
