<?php

namespace Artel\Support\Exceptions;

class ModelFactoryNotFoundedException extends EntityCreateException
{
}
