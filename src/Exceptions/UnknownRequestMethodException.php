<?php

namespace Artel\Support\Exceptions;

class UnknownRequestMethodException extends EntityCreateException
{
}
