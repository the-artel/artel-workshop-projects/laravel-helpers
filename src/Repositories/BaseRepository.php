<?php

namespace Artel\Support\Repositories;

use Artel\Support\Traits\EntityControlTrait;

class BaseRepository
{
    use EntityControlTrait;
}
