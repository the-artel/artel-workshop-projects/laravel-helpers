<?php

namespace Artel\Support\Tests;

use Illuminate\Foundation\Application;
use Artel\Support\Traits\FixturesTrait;
use Illuminate\Foundation\Testing\TestCase as BaseTest;

class HelpersTestCase extends BaseTest
{
    use FixturesTrait;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication(): Application
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }
}
